﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace DAL
{
    public class MP_empleado
    {
        private Acceso acceso = new Acceso();

        public List<BE.BEempleado> ListarTodos()
        {
            List<BEempleado> lista = new List<BEempleado>();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listar_empleado");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BEempleado empleado = new BEempleado();
                empleado.IDEMPLEADO= int.Parse(registro["idLegajo"].ToString());
                empleado.NOMYAPE = registro["nombreyapellido"].ToString();
                empleado.CUIL = decimal.Parse(registro["cuil"].ToString());
                empleado.FECHAALTA = DateTime.Parse(registro["fechaalta"].ToString());
                

                lista.Add(empleado);
            }

            return lista;
        }

        public void Insertar (BEempleado empleado)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            acceso.Abrir();
            parameters.Add(acceso.CrearParametro("@nombreyapellido", empleado.NOMYAPE));
            parameters.Add(acceso.CrearParametro("@fechaAlta", empleado.FECHAALTA));
            parameters.Add(acceso.CrearParametro("@cuil", empleado.CUIL));           
            acceso.Escribir("insertar_empleado", parameters);
            acceso.Cerrar();
        }

        public void Modificar (BEempleado empleado)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            acceso.Abrir();
            parameters.Add(acceso.CrearParametro("@idLegajo", empleado.IDEMPLEADO));
            parameters.Add(acceso.CrearParametro("@nombreyapellido", empleado.NOMYAPE));
            parameters.Add(acceso.CrearParametro("@fechaAlta", empleado.FECHAALTA));
            parameters.Add(acceso.CrearParametro("@cuil", empleado.CUIL));
            acceso.Escribir("editar_empleado", parameters);
            acceso.Cerrar();
        }

        public void Borrar (BEempleado empleado)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            acceso.Abrir();
            parameters.Add(acceso.CrearParametro("@idLegajo", empleado.IDEMPLEADO));
            
            acceso.Escribir("borrar_empleado", parameters);
            acceso.Cerrar();
        }

    }
}
