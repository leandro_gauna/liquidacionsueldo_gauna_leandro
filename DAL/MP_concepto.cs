﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using BE;

namespace DAL
{
    public class MP_concepto
    {
        private Acceso acceso = new Acceso();

        public List<BE.BEconcepto> ListarTodos()
        {
            List<BEconcepto> lista = new List<BEconcepto>();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listar_concepto");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BEconcepto unConcepto = new BEconcepto();
                unConcepto.IDCONCEPTO = int.Parse(registro["idConcepto"].ToString());
                unConcepto.DESCRIPCION = registro["descripcion"].ToString();
                unConcepto.PORCENTAJE = float.Parse(registro["porcentaje"].ToString());
                unConcepto.SIGNO = int.Parse(registro["signo"].ToString());
                

                lista.Add(unConcepto);
            }

            return lista;
        }

        public void Insertar(BEconcepto unConcepto)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            acceso.Abrir();
            parameters.Add(acceso.CrearParametro("@descripcion", unConcepto.DESCRIPCION));
            parameters.Add(acceso.CrearParametro("@porcentaje", unConcepto.PORCENTAJE));
            parameters.Add(acceso.CrearParametro("@signo", unConcepto.SIGNO));
            acceso.Escribir("insertar_concepto", parameters);
            acceso.Cerrar();
        }

        public void Modificar(BEconcepto unConcepto)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            acceso.Abrir();
            parameters.Add(acceso.CrearParametro("@idConcepto", unConcepto.IDCONCEPTO));
            parameters.Add(acceso.CrearParametro("@descripcion", unConcepto.DESCRIPCION));
            parameters.Add(acceso.CrearParametro("@porcentaje", unConcepto.PORCENTAJE));
            parameters.Add(acceso.CrearParametro("@signo", unConcepto.SIGNO));

            acceso.Escribir("editar_concepto", parameters);
            acceso.Cerrar();
        }

        public void Borrar(BEconcepto unConcepto)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            acceso.Abrir();
            parameters.Add(acceso.CrearParametro("@idConcepto", unConcepto.IDCONCEPTO));

            acceso.Escribir("borrar_concepto", parameters);
            acceso.Cerrar();
        }

        //public List<BE.BEconcepto> Liquidar()
        //{
        //    List<BEconcepto> lista = new List<BEconcepto>();
        //    acceso.Abrir();
        //    DataTable tabla = acceso.Leer("listar_concepto");
        //    acceso.Cerrar();

        //    foreach (DataRow registro in tabla.Rows)
        //    {
        //        BEconcepto unConcepto = new BEconcepto();
        //        decimal signo = unConcepto.SIGNO = decimal.Parse(registro["signo"].ToString());
        //        float porcentaje = unConcepto.PORCENTAJE = float.Parse(registro["porcentaje"].ToString());

        //        if (signo == 1)
        //        {
        //            porcentaje += porcentaje;
        //        }
                


        //        lista.Add(unConcepto);
                
        //    }

        //    return lista;
        //}

    }
}

