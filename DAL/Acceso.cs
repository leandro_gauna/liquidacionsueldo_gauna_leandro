﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms.VisualStyles;

namespace DAL
{
     class Acceso
    {
        private SqlConnection conexion;

        public bool Abrir()
        {
            bool ok;

            if (conexion != null && conexion.State == System.Data.ConnectionState.Open)
            {
                ok = true;
            }
            else
            {
                conexion = new SqlConnection();

                try
                {
                    conexion.ConnectionString = @"Data Source =.\SqlExpress; Initial Catalog=Liquidacion; Integrated Security= SSPI";
                    conexion.Open();
                    ok = true;
                }
                catch (Exception ex)
                {
                    ok = false;                  
                }
            }

            return ok;

        }
        public void Cerrar()
    
        {
            if (conexion != null)
            {
                conexion.Close();
                conexion.Dispose();
                conexion = null;
                GC.Collect();
            }
        }

        private SqlCommand CrearComando(string sql, List<SqlParameter> parametros = null, CommandType tipo = CommandType.Text)
        {

            SqlCommand comando = new SqlCommand(sql);
            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }
            comando.Connection = conexion;
            return comando;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.String;
            return p;
        }

        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Int32;
            return p;
        }
        public SqlParameter CrearParametro(string nombre, decimal valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Decimal;
            return p;
        }
        public SqlParameter CrearParametro(string nombre, float valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Double;
            return p;
        }
        public SqlParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Date;
            return p;
        }

        internal SqlParameter CrearParametro(string nombre, BE.BEempleado valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Int32;
            return p;
        }


        public int Escribir(string sql, List<SqlParameter> parameters = null)
        {
            SqlCommand comando = CrearComando(sql, parameters, CommandType.StoredProcedure);


            int filasAfectadas = 0;
            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {

                filasAfectadas = -1;
            }
            catch (Exception ex)
            {
                filasAfectadas = -2;
            }

            return filasAfectadas;
        }

        public DataTable Leer(string sql, List<SqlParameter> parameters = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CrearComando(sql, parameters);

            DataTable Tabla = new DataTable();

            adaptador.Fill(Tabla);


            return Tabla;
        }

       
        public bool ComprobarConexión(string SQL)
        {
            bool ok;
            
            if (conexion != null && conexion.State == System.Data.ConnectionState.Open)
            {
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;


        }
        public SqlDataReader LeerConectado(string SQL)
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandText = SQL;
            comando.CommandType = System.Data.CommandType.Text;
            SqlDataReader lector = comando.ExecuteReader();

            return lector;
        }
        public int LeerEscalar(string SQL)
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandText = SQL;
            comando.CommandType = System.Data.CommandType.Text;
            int resultado = int.Parse(comando.ExecuteScalar().ToString());

            return resultado;

        }


    }
}
