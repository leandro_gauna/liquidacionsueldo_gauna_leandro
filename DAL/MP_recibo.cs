﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MP_recibo
    {

        Acceso acceso = new Acceso();

        public List<BE.BEempleado> ListarEmpleados()
        {
            List<BEempleado> lista = new List<BEempleado>();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listar_empleado");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BEempleado empleado = new BEempleado();
                empleado.IDEMPLEADO = int.Parse(registro["idLegajo"].ToString());
                empleado.NOMYAPE = registro["nombreyapellido"].ToString();
                empleado.CUIL = decimal.Parse(registro["cuil"].ToString());
                empleado.FECHAALTA = DateTime.Parse(registro["fechaalta"].ToString());


                lista.Add(empleado);
            }

            return lista;
        }
        public List<BE.BEconcepto> ListarConcepto()
        {
            List<BEconcepto> lista = new List<BEconcepto>();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listar_concepto");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BEconcepto unConcepto = new BEconcepto();
                unConcepto.IDCONCEPTO = int.Parse(registro["idConcepto"].ToString());
                unConcepto.DESCRIPCION = registro["descripcion"].ToString();
                unConcepto.PORCENTAJE = float.Parse(registro["porcentaje"].ToString());
                unConcepto.SIGNO = int.Parse(registro["signo"].ToString());


                lista.Add(unConcepto);
            }

            return lista;
        }

        public void Insertar(BErecibo recibo)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            acceso.Abrir();

            parameters.Add(acceso.CrearParametro("@idLegajo", recibo.IDEMPLEADO));
            parameters.Add(acceso.CrearParametro("@mesanio", recibo.MESANIO));
            parameters.Add(acceso.CrearParametro("@sueldoBruto", recibo.SUELDOBRUTO));
            parameters.Add(acceso.CrearParametro("@sueldoNeto", recibo.SUELDONETO));
            parameters.Add(acceso.CrearParametro("@totalDescuento", recibo.TOTALDESCUENTO));
            acceso.Escribir("insertar_recibo", parameters);
            acceso.Cerrar();
        }


        public List<BE.BErecibo> ListarRecibos()
        {
            List<BErecibo> lista = new List<BErecibo>();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listar_recibo");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BErecibo unrecibo = new BErecibo();
                unrecibo.IDRECIBO = int.Parse(registro["idRecibo"].ToString());
                unrecibo.IDEMPLEADO = int.Parse(registro["idLegajo"].ToString());
                unrecibo.MESANIO = DateTime.Parse(registro["mesanio"].ToString());
                unrecibo.SUELDOBRUTO = float.Parse(registro["sueldoBruto"].ToString());
                unrecibo.SUELDONETO = float.Parse(registro["sueldoNeto"].ToString());
                unrecibo.TOTALDESCUENTO = float.Parse(registro["totalDescuento"].ToString());
                lista.Add(unrecibo);
            }

            return lista;
        }

    }
}
