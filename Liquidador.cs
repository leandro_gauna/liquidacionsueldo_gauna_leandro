﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using BE;
using BLL;

namespace LiquidacionSueldo_Gauna_Leandro
{
    public partial class Liquidador : Form
    {
        public Liquidador()
        {
            InitializeComponent();
            
        }

        BLLrecibo gestor = new BLLrecibo();
        BEempleado empleado = new BEempleado();
        private void Liquidador_Activated(object sender, EventArgs e)
        {
            gestor.ListarConcepto();
            gestor.ListarEmpleados();
        }

        private void Liquidador_Load(object sender, EventArgs e)
        {
            Enlazar();
            
        }

        public void Enlazar()
        {
           
            dgvEmpleado.DataSource = null;
            dgvEmpleado.DataSource = gestor.ListarEmpleados();
            dgvRecibo.DataSource = null;
            dgvRecibo.DataSource = gestor.ListarRecibos();
        }

        private void dgvConcepto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
           
        }

        private void cmbAnio_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        
        
        private void btnLiquidar_Click(object sender, EventArgs e)
        {
            BErecibo unRecibo = new BErecibo();
            
            unRecibo.IDEMPLEADO = int.Parse(dgvEmpleado.SelectedRows[0].Cells[0].Value.ToString());
            unRecibo.MESANIO = dateTimePicker1.Value;
            
            if (string.IsNullOrWhiteSpace(txtSueldo.Text))
            {
                MessageBox.Show("Debe ingresar el sueldo bruto");
            }
            else
            {
                unRecibo.SUELDOBRUTO = float.Parse(txtSueldo.Text);
                float totalDesc = 0;
                float totalSum = 0;
                float sdoneto = 0;
                float total = 0;
                List<BEconcepto> lista = gestor.ListarConcepto();
                foreach (BEconcepto unconcepto in lista)
                {
                    if (unconcepto.SIGNO == 1)
                    {
                        totalSum += (float.Parse(txtSueldo.Text)) * unconcepto.PORCENTAJE / (100);
                    }
                    else
                    {
                        totalDesc = +(float.Parse(txtSueldo.Text)) * unconcepto.PORCENTAJE / (100);
                    }

                    total = totalSum - totalDesc;

                }

                sdoneto = +float.Parse(txtSueldo.Text) + total;

                unRecibo.SUELDONETO = sdoneto;
                unRecibo.TOTALDESCUENTO = totalDesc;
                gestor.Insertar(unRecibo);
                Enlazar();
            }


           

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
          
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            List<BE.BErecibo> lista = gestor.ListarRecibos();
            var recibos = (from BE.BErecibo unrecibo in lista
                     where unrecibo.IDEMPLEADO == int.Parse(dgvEmpleado.SelectedRows[0].Cells[0].Value.ToString())
                     select unrecibo
                     );

            dgvRecibo.DataSource = null;
            dgvRecibo.DataSource = recibos.ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void txtSueldo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
             if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {

                e.Handled = true;
            }
        }
    }
}
