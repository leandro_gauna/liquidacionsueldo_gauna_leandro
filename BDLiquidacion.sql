USE [master]
GO
/****** Object:  Database [Liquidacion]    Script Date: 30/9/2020 09:47:45 ******/
CREATE DATABASE [Liquidacion]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LIquidacion', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\LIquidacion.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LIquidacion_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\LIquidacion_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Liquidacion] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Liquidacion].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Liquidacion] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Liquidacion] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Liquidacion] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Liquidacion] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Liquidacion] SET ARITHABORT OFF 
GO
ALTER DATABASE [Liquidacion] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Liquidacion] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Liquidacion] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Liquidacion] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Liquidacion] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Liquidacion] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Liquidacion] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Liquidacion] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Liquidacion] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Liquidacion] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Liquidacion] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Liquidacion] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Liquidacion] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Liquidacion] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Liquidacion] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Liquidacion] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Liquidacion] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Liquidacion] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Liquidacion] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Liquidacion] SET  MULTI_USER 
GO
ALTER DATABASE [Liquidacion] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Liquidacion] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Liquidacion] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Liquidacion] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Liquidacion]
GO
/****** Object:  StoredProcedure [dbo].[borrar_concepto]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[borrar_concepto] 
@idConcepto int 


as
BEGIN
delete Concepto where idConcepto = @idConcepto
 
END
GO
/****** Object:  StoredProcedure [dbo].[borrar_empleado]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[borrar_empleado] 
@idLegajo int 


as
BEGIN
delete Empleado where idLegajo = @idLegajo
 
END
GO
/****** Object:  StoredProcedure [dbo].[editar_concepto]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[editar_concepto] 
@idConcepto int, 
@descripcion varchar(50),
@porcentaje float,
@signo smallint


as
BEGIN
update Concepto set
descripcion = @descripcion,
porcentaje = @porcentaje, 
signo = @signo

where @idConcepto = @idConcepto
 
END
GO
/****** Object:  StoredProcedure [dbo].[editar_empleado]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[editar_empleado] 
@idLegajo int, 
@nombreyapellido varchar(50),
@cuil varchar(11),
@fechaAlta date

as
BEGIN
update Empleado set
nombreyapellido = @nombreyapellido,
cuil = @cuil, 
fechaAlta = @fechaAlta

where idLegajo = @idLegajo
 
END
GO
/****** Object:  StoredProcedure [dbo].[insertar_concepto]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[insertar_concepto] 
@descripcion varchar(50),
@porcentaje float,
@signo smallint


as
BEGIN

Declare @idConcepto int 

set @idConcepto  = (SELECT ISNULL ( MAX(idConcepto) , 0) +1 FROM Concepto) 



insert into Concepto values(@idConcepto,@descripcion,@porcentaje, @signo)
 
END


GO
/****** Object:  StoredProcedure [dbo].[insertar_empleado]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[insertar_empleado] 
@nombreyapellido varchar(50),
@cuil varchar(11),
@fechaAlta date

as
BEGIN

Declare @idLegajo int 

set @idLegajo  = (SELECT ISNULL ( MAX(idLegajo) , 0) + 1 FROM Empleado) 
--agregamos el select de obtener ID todo entre parentesis.


insert into Empleado values(@idLegajo,@nombreyapellido,@cuil,@fechaAlta)
 
END


GO
/****** Object:  StoredProcedure [dbo].[insertar_recibo]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[insertar_recibo] 
@idlegajo int,
@mesanio date,
@sueldoBruto float,
@sueldoNeto float,
@totalDescuento float

as
BEGIN

Declare @idRecibo int 

set @idRecibo  = (SELECT ISNULL ( MAX(idRecibo) , 0) + 1 FROM Recibo) 

insert into Recibo values(@idRecibo,@idlegajo,@mesanio,@sueldoBruto,@sueldoNeto,@totalDescuento)
 
END


GO
/****** Object:  StoredProcedure [dbo].[listar_concepto]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[listar_concepto] 


as
BEGIN

select * from Concepto
 
END


GO
/****** Object:  StoredProcedure [dbo].[listar_empleado]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[listar_empleado] 


as
BEGIN

select * from Empleado
 
END


GO
/****** Object:  StoredProcedure [dbo].[listar_recibo]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[listar_recibo] 


as
BEGIN

select r.idRecibo, r.idLegajo, r.mesanio,r.sueldoBruto,r.sueldoNeto,r.totalDescuento from Recibo as r
 
END


GO
/****** Object:  Table [dbo].[Concepto]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Concepto](
	[idConcepto] [int] NOT NULL,
	[descripcion] [varchar](50) NULL,
	[porcentaje] [float] NULL,
	[signo] [int] NULL,
 CONSTRAINT [PK_Concepto] PRIMARY KEY CLUSTERED 
(
	[idConcepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleado](
	[idLegajo] [int] NOT NULL,
	[nombreyapellido] [varchar](50) NULL,
	[cuil] [decimal](11, 0) NULL,
	[fechaAlta] [date] NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[idLegajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Recibo]    Script Date: 30/9/2020 09:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Recibo](
	[idRecibo] [int] NOT NULL,
	[idLegajo] [int] NULL,
	[mesanio] [varchar](50) NULL,
	[sueldoBruto] [float] NULL,
	[sueldoNeto] [float] NULL,
	[totalDescuento] [float] NULL,
 CONSTRAINT [PK_Recibo] PRIMARY KEY CLUSTERED 
(
	[idRecibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Recibo]  WITH CHECK ADD  CONSTRAINT [FK_Recibo_Empleado] FOREIGN KEY([idLegajo])
REFERENCES [dbo].[Empleado] ([idLegajo])
GO
ALTER TABLE [dbo].[Recibo] CHECK CONSTRAINT [FK_Recibo_Empleado]
GO
USE [master]
GO
ALTER DATABASE [Liquidacion] SET  READ_WRITE 
GO
