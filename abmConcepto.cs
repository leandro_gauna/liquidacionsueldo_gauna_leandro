﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace LiquidacionSueldo_Gauna_Leandro
{
    public partial class abmConcepto : Form
    {
        public abmConcepto()
        {
            InitializeComponent();
            Enlazar();
        }

        BLLconcepto gestor = new BLLconcepto();

        private void btnAlta_Click(object sender, EventArgs e)
        {
            BEconcepto concepto = new BEconcepto();
            
            concepto.DESCRIPCION = txtDescripcion.Text;
            concepto.PORCENTAJE = float.Parse(txtPorcentaje.Text);
            if(rdbPositivo.Checked == true)
            {
                concepto.SIGNO = 1;
            }
            if (rdbNegativo.Checked == true)
            {
                concepto.SIGNO = -1;
            }

            gestor.Insertar(concepto);

            txtDescripcion.Text = " ";
            txtPorcentaje.Text = " ";
            rdbNegativo.Checked = false;
            rdbPositivo.Checked = false;

            Enlazar();
        }

        public void Enlazar()
        {
            dgvConcepto.DataSource = null;
            dgvConcepto.DataSource = gestor.Listar();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            BEconcepto unConcepto = (BEconcepto)dgvConcepto.SelectedRows[0].DataBoundItem;
            unConcepto.DESCRIPCION=txtDescripcion.Text;
            unConcepto.PORCENTAJE = float.Parse(txtPorcentaje.Text);

            if (rdbPositivo.Checked == true)
            {
                unConcepto.SIGNO = 1;
            }
            if (rdbNegativo.Checked == true)
            {
                unConcepto.SIGNO = -1;
            }

            gestor.Modificar(unConcepto);

            Enlazar();
        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            BEconcepto unConcepto = (BEconcepto)dgvConcepto.SelectedRows[0].DataBoundItem;

            gestor.Borrar(unConcepto);

            Enlazar();
        }

        private void dgvConcepto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtPorcentaje.Text =  dgvConcepto.SelectedRows[0].Cells[2].Value.ToString();
            txtDescripcion.Text = dgvConcepto.SelectedRows[0].Cells[1].Value.ToString();
            if (int.Parse(dgvConcepto.SelectedRows[0].Cells[3].Value.ToString()) == 1 )
            {
                rdbPositivo.Checked = true;
            }
            if (int.Parse(dgvConcepto.SelectedRows[0].Cells[3].Value.ToString()) == -1)
            {
                rdbNegativo.Checked = true;
            }


        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPorcentaje_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
             if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {

                e.Handled = true;
            }
        }
    }
}
