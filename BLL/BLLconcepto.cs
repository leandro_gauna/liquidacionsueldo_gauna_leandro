﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BE;

namespace BLL
{
    public class BLLconcepto
    {
        MP_concepto mp = new MP_concepto();

        public void Insertar(BEconcepto unConcepto)
        {
            mp.Insertar(unConcepto);
        }

        public List<BEconcepto> Listar()
        {
            return mp.ListarTodos();
        }

        public void Modificar(BEconcepto unConcepto)
        {
            mp.Modificar(unConcepto);
        }
        public void Borrar(BEconcepto unConcepto)
        {
            mp.Borrar(unConcepto);
        }

    }
}

