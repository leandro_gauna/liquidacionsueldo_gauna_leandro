﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace BLL
{
    public class BLLrecibo
    {
        MP_recibo mp = new MP_recibo();

        public void Insertar(BErecibo unRecibo)
        {
          mp.Insertar(unRecibo);
        }

        public List<BEconcepto> ListarConcepto()
        {
            return mp.ListarConcepto();
           
        }
        public List<BEconcepto> Liquidacion()
        {
            return mp.ListarConcepto();

        }

        public List<BEempleado> ListarEmpleados()
        {
            return mp.ListarEmpleados();
        }
        public List<BErecibo> ListarRecibos()
        {
            return mp.ListarRecibos();

        }
        

    }
}
