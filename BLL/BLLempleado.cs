﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;


namespace BLL
{
    public class BLLempleado
    {
        MP_empleado mp = new MP_empleado();

        public void Insertar( BEempleado empleado)
        {
            mp.Insertar(empleado);
        }

        public List<BEempleado> Listar()
        {
           return mp.ListarTodos();
        }

        public void Modificar(BEempleado empleado)
        {
            mp.Modificar(empleado);
        }
        public void Borrar(BEempleado empleado)
        {
            mp.Borrar(empleado);
        }

    }
}
