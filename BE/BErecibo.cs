﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BErecibo
    {
        private int idRecibo;

        public int IDRECIBO
        {
            get { return idRecibo; }
            set { idRecibo = value; }
        }

        
        private int idEmpleado;

        public int IDEMPLEADO
        {
            get { return idEmpleado; }
            set { idEmpleado = value; }
        }


        private List<BEconcepto> concepto;

        public List<BEconcepto> CONCEPTO
        {
            get { return concepto; }
            set { concepto = value; }
        }


        private DateTime mesanio;

        public DateTime MESANIO
        {
            get { return mesanio; }
            set { mesanio = value; }
        }

        private float sueldobruto;

        public float SUELDOBRUTO
        {
            get { return sueldobruto; }
            set { sueldobruto = value; }
        }

        private float sueldoneto;

        public float SUELDONETO
        {
            get { return sueldoneto; }
            set { sueldoneto = value; }
        }

        private float totaldescuento;

        public float TOTALDESCUENTO
        {
            get { return totaldescuento; }
            set { totaldescuento = value; }
        }


    }
}
