﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BEempleado
    {
        private int idEmpleado;
        public int IDEMPLEADO
        {
            get { return idEmpleado; }
            set { idEmpleado = value; }
        }

        private string nomyape;

        public string NOMYAPE
        {
            get { return nomyape; }
            set { nomyape = value; }
        }

        private Decimal  cuil;
        public Decimal CUIL
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechaalta;         
        public DateTime FECHAALTA
        {
            get { return fechaalta; }
            set { fechaalta= value; }
        }

        

    }
}
