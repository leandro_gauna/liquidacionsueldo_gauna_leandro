﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BEconcepto
    {
        private int idConcepto;

        public int IDCONCEPTO
        {
            get { return idConcepto; }
            set { idConcepto = value; }
        }

        private string descripcion;

        public string DESCRIPCION
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private float porcentaje;

        public float PORCENTAJE
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }

        private int signo;

        public int SIGNO
        {
            get { return signo; }
            set { signo = value; }
        }

    }
}
