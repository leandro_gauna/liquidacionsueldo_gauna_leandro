﻿namespace LiquidacionSueldo_Gauna_Leandro
{
    partial class abmConcepto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnBaja = new System.Windows.Forms.Button();
            this.btnAlta = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtPorcentaje = new System.Windows.Forms.TextBox();
            this.dgvConcepto = new System.Windows.Forms.DataGridView();
            this.rdbPositivo = new System.Windows.Forms.RadioButton();
            this.rdbNegativo = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConcepto)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(429, 386);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 33);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "SALIR";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(394, 116);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(110, 33);
            this.btnModificar.TabIndex = 6;
            this.btnModificar.Text = "MODIFICAR";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnBaja
            // 
            this.btnBaja.Location = new System.Drawing.Point(405, 78);
            this.btnBaja.Name = "btnBaja";
            this.btnBaja.Size = new System.Drawing.Size(99, 32);
            this.btnBaja.TabIndex = 5;
            this.btnBaja.Text = "BAJA";
            this.btnBaja.UseVisualStyleBackColor = true;
            this.btnBaja.Click += new System.EventHandler(this.btnBaja_Click);
            // 
            // btnAlta
            // 
            this.btnAlta.Location = new System.Drawing.Point(405, 36);
            this.btnAlta.Name = "btnAlta";
            this.btnAlta.Size = new System.Drawing.Size(99, 36);
            this.btnAlta.TabIndex = 4;
            this.btnAlta.Text = "ALTA";
            this.btnAlta.UseVisualStyleBackColor = true;
            this.btnAlta.Click += new System.EventHandler(this.btnAlta_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Descripción";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Porcentaje ";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(176, 46);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(182, 26);
            this.txtDescripcion.TabIndex = 10;
            // 
            // txtPorcentaje
            // 
            this.txtPorcentaje.Location = new System.Drawing.Point(176, 84);
            this.txtPorcentaje.Name = "txtPorcentaje";
            this.txtPorcentaje.Size = new System.Drawing.Size(182, 26);
            this.txtPorcentaje.TabIndex = 11;
            this.txtPorcentaje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPorcentaje_KeyPress);
            // 
            // dgvConcepto
            // 
            this.dgvConcepto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConcepto.Location = new System.Drawing.Point(45, 213);
            this.dgvConcepto.Name = "dgvConcepto";
            this.dgvConcepto.RowHeadersWidth = 62;
            this.dgvConcepto.RowTemplate.Height = 28;
            this.dgvConcepto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvConcepto.Size = new System.Drawing.Size(348, 206);
            this.dgvConcepto.TabIndex = 12;
            this.dgvConcepto.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConcepto_CellClick);
            // 
            // rdbPositivo
            // 
            this.rdbPositivo.AutoSize = true;
            this.rdbPositivo.Location = new System.Drawing.Point(176, 125);
            this.rdbPositivo.Name = "rdbPositivo";
            this.rdbPositivo.Size = new System.Drawing.Size(88, 24);
            this.rdbPositivo.TabIndex = 15;
            this.rdbPositivo.TabStop = true;
            this.rdbPositivo.Text = "Positivo";
            this.rdbPositivo.UseVisualStyleBackColor = true;
            // 
            // rdbNegativo
            // 
            this.rdbNegativo.AutoSize = true;
            this.rdbNegativo.Location = new System.Drawing.Point(176, 155);
            this.rdbNegativo.Name = "rdbNegativo";
            this.rdbNegativo.Size = new System.Drawing.Size(96, 24);
            this.rdbNegativo.TabIndex = 16;
            this.rdbNegativo.TabStop = true;
            this.rdbNegativo.Text = "Negotivo";
            this.rdbNegativo.UseVisualStyleBackColor = true;
            // 
            // abmConcepto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 450);
            this.Controls.Add(this.rdbNegativo);
            this.Controls.Add(this.rdbPositivo);
            this.Controls.Add(this.dgvConcepto);
            this.Controls.Add(this.txtPorcentaje);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnBaja);
            this.Controls.Add(this.btnAlta);
            this.Name = "abmConcepto";
            this.Text = "abmConcepto";
            ((System.ComponentModel.ISupportInitialize)(this.dgvConcepto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnBaja;
        private System.Windows.Forms.Button btnAlta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtPorcentaje;
        private System.Windows.Forms.DataGridView dgvConcepto;
        private System.Windows.Forms.RadioButton rdbPositivo;
        private System.Windows.Forms.RadioButton rdbNegativo;
    }
}