﻿namespace LiquidacionSueldo_Gauna_Leandro
{
    partial class RecibosEmpleados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvRecibosEmpleados = new System.Windows.Forms.DataGridView();
            this.btnSalir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecibosEmpleados)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvRecibosEmpleados
            // 
            this.dgvRecibosEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecibosEmpleados.Location = new System.Drawing.Point(54, 37);
            this.dgvRecibosEmpleados.Name = "dgvRecibosEmpleados";
            this.dgvRecibosEmpleados.RowHeadersWidth = 62;
            this.dgvRecibosEmpleados.RowTemplate.Height = 28;
            this.dgvRecibosEmpleados.Size = new System.Drawing.Size(672, 358);
            this.dgvRecibosEmpleados.TabIndex = 0;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(536, 402);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(190, 36);
            this.btnSalir.TabIndex = 1;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // RecibosEmpleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.dgvRecibosEmpleados);
            this.Name = "RecibosEmpleados";
            this.Text = "RecibosEmpleados";
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecibosEmpleados)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRecibosEmpleados;
        private System.Windows.Forms.Button btnSalir;
    }
}