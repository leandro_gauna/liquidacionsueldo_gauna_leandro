﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LiquidacionSueldo_Gauna_Leandro
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void altaEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            abmEmpleado frm = new abmEmpleado();
            frm.MdiParent=this;
            frm.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void altaConceptosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            abmConcepto frm = new abmConcepto();
            frm.MdiParent = this;
            frm.Show();
        }

        private void liquidarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Liquidador frm = new Liquidador();
            frm.MdiParent = this;
            frm.Show();

        }
    }
}
