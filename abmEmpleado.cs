﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;


namespace LiquidacionSueldo_Gauna_Leandro
{
    public partial class abmEmpleado : Form
    {
        public abmEmpleado()
        {
            InitializeComponent();
            Enlazar();
        }

        BLLempleado gestor = new BLLempleado();
        BEempleado unEmpleado = new BEempleado();
        private void btnAlta_Click(object sender, EventArgs e)
        {
                   
            
            unEmpleado.NOMYAPE = txtNyA.Text;
            unEmpleado.CUIL = decimal.Parse(txtCuil.Text);
            unEmpleado.FECHAALTA = dtpFechaalta.Value;

            gestor.Insertar(unEmpleado);

            txtNyA.Text = " ";
            txtCuil.Text = " ";
            Enlazar();
            
        }

        public void Enlazar()
        {
            dgvEmpleados.DataSource = null;
            dgvEmpleados.DataSource = gestor.Listar();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            BEempleado unEmpleado =(BEempleado)dgvEmpleados.SelectedRows[0].DataBoundItem;
            unEmpleado.NOMYAPE = txtNyA.Text;
            unEmpleado.CUIL = decimal.Parse(txtCuil.Text);
            unEmpleado.FECHAALTA = dtpFechaalta.Value;

            gestor.Modificar(unEmpleado);

            Enlazar();

        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            BEempleado unEmpleado = (BEempleado)dgvEmpleados.SelectedRows[0].DataBoundItem;
         
            gestor.Borrar(unEmpleado);

            Enlazar();
        }

        private void dgvEmpleados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNyA.Text = dgvEmpleados.SelectedRows[0].Cells[1].Value.ToString();
            txtCuil.Text = dgvEmpleados.SelectedRows[0].Cells[2].Value.ToString();
        }

        private void txtCuil_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
               
                e.Handled = true;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
